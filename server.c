    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>    
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <time.h>
    #include <math.h>

#define CZAS 1004 //protokół do uzyskania czasu serwera
#define ROWNANIE 1005  //protokół do rozwiązywania równania kwadratowego



int
main ()
{	
	time_t t;
        time(&t);

	int server_sockfd, client_sockfd;
	socklen_t server_len, client_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;

	server_sockfd = socket (AF_INET, SOCK_STREAM, 0);

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl (INADDR_ANY);
	server_address.sin_port = htons (5000);
	server_len = sizeof (server_address);
	bind (server_sockfd, (struct sockaddr *) &server_address, server_len);

	listen (server_sockfd, 5);

	signal (SIGCHLD, SIG_IGN);

		//sprawdzanie wartosci ednian
	int i = 1;
	int endian;
    	char *tp = (char *)&i;

 	if (tp[0] == 1){
 	endian=0;}//jeśli 1 na pierwszym miejscu to litle endian       	
    	else{
	endian=1;}//jeśli 0 na pierwszym miejscu to big endian 

	while (1)
	{        	

		int znak,a,b,c;

		printf ("Serwer w trybie oczekiwania\n");


		client_len = sizeof (client_address);
		client_sockfd = accept (server_sockfd,
				(struct sockaddr *) &client_address,
				&client_len);

		if (!fork())
		{

			for(;;){

				if((read(client_sockfd, &znak,sizeof(znak)))==-1){
					perror("Odbiór danych");
             				exit(1);
				}

				if(znak == CZAS){
					write(client_sockfd, ctime(&t), 24);
				}else if(znak == ROWNANIE){
					if((read(client_sockfd, &a,sizeof(a)))==-1){
						perror("Odbiór danych");
	             				exit(1);
					}
					if((read(client_sockfd, &b,sizeof(b)))==-1){
						perror("Odbiór danych");
	             				exit(1);
					}
					if((read(client_sockfd, &c,sizeof(c)))==-1){
						perror("Odbiór danych");
	             				exit(1);
					}
					
					int ilosc,ilosc2;
					float delta,x,x1,x2;

					delta = ((float)b*(float)b) - (4*(float)a*(float)c);
					
					if(delta < 0){
						ilosc=0;
					}
					else if(delta == 0){	
						ilosc=1;				
					}
					else{
						ilosc=2;
					}
					
					//konwersja z little do big endian
					int tmp; 
					if(endian==0){
						char *endian_stary = (char *)&ilosc; 
						char *endian_nowy = (char *)&tmp; 
						endian_nowy[0] = endian_stary[3]; 
						endian_nowy[1] = endian_stary[2]; 
						endian_nowy[2] = endian_stary[1]; 
						endian_nowy[3] = endian_stary[0];  
						ilosc=tmp;					
					}
	
					write(client_sockfd, &tmp, sizeof(ilosc));

					if(delta > 0){
						x1=(((float)b*(-1))-sqrt(delta))/(2*(float)a);
						write(client_sockfd, &x1, sizeof(x1));

						x2=(((float)b*(-1))+sqrt(delta))/(2*(float)a);
						write(client_sockfd, &x2, sizeof(x2));


					}
					else if(delta == 0){

						x=((float)b*(-1))/(2*(float)a);
						write(client_sockfd, &x, sizeof(x));
						
					}else{			
					}
					
				}         			
					
			}
			
			
		}


		else
		{
			close (client_sockfd);
		}
	}
}