    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <errno.h>
    #include <string.h>
    #include <sys/types.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <arpa/inet.h>
    #include <sys/wait.h>
    #include <signal.h>
    #include <time.h>

#define CZAS 1004 //protokół do uzyskania czasu serwera
#define ROWNANIE 1005  //protokół do rozwiązywania równania kwadratowego

int
main ()
{
	int sockfd;
	socklen_t len;
	struct sockaddr_in address;
	int result;
	int znak,a,b,c;

	sockfd = socket (AF_INET, SOCK_STREAM, 0);

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	address.sin_port = htons (5000);
	len = sizeof (address);


	result = connect (sockfd, (struct sockaddr *) &address, len);

	if (result == -1)
	{
		perror ("Błąd: program Client");
		exit (0);
	}
		
	//sprawdzanie wartosci edniana
	int i = 1;
	int endian;
    	char *tp = (char *)&i;

 	if (tp[0] == 1){
 	endian=0;}//jeśli 1 na pierwszym miejscu to litle endian       	
    	else{
	endian=1;}//jeśli 0 na pierwszym miejscu to big endian 
	
		printf("Wpisz 1004 by sprawdzić czas\n");
		printf("Wpisz 1005 by przejść do obliczania pierwiastków kwadratowych\n");
	

	for(;;){

		printf("Podaj komendę: ");
		scanf("%d",&znak);

		write(sockfd, &znak, sizeof(znak));

		if(znak == CZAS){
			char czas[24]="";
			read(sockfd, &czas,24);
			printf ("Czas pobrany z serwera = %s\n", czas);
		}else if(znak == ROWNANIE){
				
			printf("Parametr A : ");
			scanf("%d",&a);
			write(sockfd, &a, sizeof(a));

			printf("Parametr B : ");
			scanf("%d",&b);
			write(sockfd, &b, sizeof(b));

			printf("Parametr C : ");
			scanf("%d",&c);
			write(sockfd, &c, sizeof(c));
				
			int ilosc;
			float x,x1,x2;
			
			read(sockfd, &ilosc,sizeof(ilosc));	
			
			int tmp; 
			if(endian==0){
				
				char *endian_stary = (char *)&ilosc; 
				char *endian_nowy = (char *)&tmp; 
				endian_nowy[0] = endian_stary[3]; 
				endian_nowy[1] = endian_stary[2]; 
				endian_nowy[2] = endian_stary[1]; 
				endian_nowy[3] = endian_stary[0]; 
				
			}				
			
				
			if(tmp == 0){
				printf("Delta jest ujemna. Równanie nie posiada rozwiązania\n");
			}else if(tmp == 1){
				read(sockfd, &x,sizeof(x));
				printf("Delta jest zerowa. Równanie posiada jedno rozwiązanie:\n x = %f \n",x);			
			}else if(tmp == 2){
				read(sockfd, &x1,sizeof(x1));
				read(sockfd, &x2,sizeof(x2));
				printf("Delta jest dodatnia. Równanie posiada dwa rozwiązania:\n x1 = %f \n x2 = %f \n", x1,x2);			
			}else{printf("Błąd serwera\n");}
				
			
		}

	}
	
	close (sockfd);
	exit (0);
}
